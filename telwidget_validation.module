<?php
/**
 * @file
 * Module code
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function telwidget_validation_form_field_ui_field_edit_form_alter(&$form, &$form_state) {
  $field = $form['#field'];
  $instance = $form['#instance'];

  if ($field['type'] == 'text' && !$field['locked']) {
    // if field don't have space to store phone number, disable validation.
    if (field_has_data($field) && $field['settings']['max_length'] < 20) {
      return;
    }
    else {
      $form['field']['settings']['max_length']['#element_validate'][] = 'telwidget_element_validate_maxlength';
    }
  }

  if ($instance['widget']['type'] == 'telwidget' && !$field['locked']) {
    if (!isset($instance['widget']['settings']['telephone_validation'])) {
      // If that's a brand new instance initialize settings array.
      $instance['widget']['settings']['telephone_validation'] = array();
    }
    $settings = $instance['widget']['settings']['telephone_validation'];
    // Ensure we have at least default values.
    $settings += _telephone_validation_get_default_settings();

    if (isset($form_state['values']['instance']['widget']['settings']['telephone_validation'])) {
      $form_settings = $form_state['values']['instance']['widget']['settings']['telephone_validation'];
    }

    if (!isset($form['instance']['widget']['settings'])) {
      // Initialize settings array.
      $form['instance']['widget']['settings'] = array(

      );
    }

    // Add parent fieldset for module settings.
    $form['instance']['widget']['settings']['telephone_validation'] = array(
      '#type' => 'fieldset',
      '#title' => t('Validation'),
    );

    // Right now format field supports E164 format only (just to be sure that
    // phone validator will work correctly).
    $form['instance']['widget']['settings']['telephone_validation']['valid_format'] = array(
      '#type' => 'select',
      '#title' => t('Valid telephone format'),
      '#description' => t('It is recommended to use E164 validation format. If you want to limit field instance to one country only you can change it to National format and choose country in next field. Otherwise, users will be required to add their telephone country code as part of the telephone number entered.'),
      '#default_value' => $settings['valid_format'],
      '#options' => array(
        \libphonenumber\PhoneNumberFormat::E164 => t('E164'),
        \libphonenumber\PhoneNumberFormat::NATIONAL => t('National'),
      ),
      '#ajax' => array(
        'callback' => '_telephone_validation_valid_countries_ajax_callback',
        'wrapper' => 'telephone-validation-valid-countries',
        'method' => 'replace',
      ),
    );

    $default_value = isset($form_settings['valid_format']) ? $form_settings['valid_format'] : $settings['valid_format'];
    $form['instance']['widget']['settings']['telephone_validation']['valid_countries'] = array(
      '#type' => 'select',
      '#title' => t('List of valid countries'),
      '#description' => t('If no country selected all countries are valid.'),
      '#default_value' => $settings['valid_countries'],
      '#multiple' => ($default_value == \libphonenumber\PhoneNumberFormat::NATIONAL) ? FALSE : TRUE,
      '#options' => _telephone_validation_get_country_codes(),
      '#prefix' => '<div id="telephone-validation-valid-countries">',
      '#suffix' => '</div>',
    );

    $form['instance']['widget']['settings']['telephone_validation']['store_format'] = array(
      '#type' => 'select',
      '#title' => t('Store format'),
      '#description' => t('It is highly recommended to store data in E164 format. That is international format with no whitespaces preceded by plus and country code. Use other formats only if you know what you are doing.'),
      '#default_value' => $settings['store_format'],
      '#options' => array(
        \libphonenumber\PhoneNumberFormat::E164 => t('E164'),
        \libphonenumber\PhoneNumberFormat::NATIONAL => t('National'),
        \libphonenumber\PhoneNumberFormat::INTERNATIONAL => t('International'),
      ),
    );
  }
}

/**
 * Form element validation handler for max length input.
 */
function telwidget_element_validate_maxlength($element, &$form_state) {
  $value = $element['#value'];
  if ($value < 20) {
    form_error($element, t('%name must be greater than %length', array('%name' => $element['#title'], '%length' => 20)));
  }
}

/**
 * Implements hook_field_widget_WIDGET_TYPE_form_alter().
 */
function telwidget_validation_field_widget_telwidget_form_alter(&$element, &$form_state, $context) {
  $field = field_widget_field($element['value'], $form_state);
  if ($field['settings']['max_length'] >= 20) {
    $element['value']['#element_validate'][] = 'telephone_validation_element';
  }
}
