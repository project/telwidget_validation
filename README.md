# Telwidget validation

Telwidget validation provides validation to telwidget for textfield.<br>
It uses [Telephone Validation](https://www.drupal.org/project/telephone_validation) module to validate a phone number.

For more information see a [Telephone Validation](https://www.drupal.org/project/telephone_validation).

## Installation

Install [Telephone Validation](https://www.drupal.org/project/telephone_validation).

After install and setup Telephone Validation and libphonenumber-for-php, you can just install this module and configure field instance settings.

## Requirements
textfield max length must greater than 20.<br>
If textfield has max length less than 20, Validation will not work.

## Dependecies
* [HTML5_Tools](https://www.drupal.org/project/html5_tools)
* [Telephone Validation](https://www.drupal.org/project/telephone_validation)
* Composer integrations (eg. [Composer Manager](https://www.drupal.org/composer_manager))
* [giggsey/libphonenumber-for-php](https://packagist.org/packages/giggsey/libphonenumber-for-php)
